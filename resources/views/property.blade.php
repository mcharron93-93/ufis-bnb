<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>UFIS-BNB</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #f6f6f6;
                color: #006a70;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                font-size: 16px;
            }

            .full-height {
                height: 100vh;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 48px;
                padding: 2rem 0rem;
            }

            .content-wrapper{
                width: 75%;
                margin: auto;
                background-color: #fff;
                padding: 2rem 0rem;
                border-radius: 2%;
                box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
            }

            .wrapper{
                font-size: 22px;
                display:flex;
                justify-content:start;
                padding: 0 3rem;
            }

            .wrapper h4{
                margin-top:0;
            }

            .review{
                display: flex;
                padding: 1.5rem 3rem;
                justify-content: space-between;
            }

            .property-name{
                padding-bottom: 3rem;
            }

            .navbar a{
                width: 100%;
                display: flex;
                padding: 1rem;
                background-color: #2b2a2a;
                color: #f1f1f1;
            }
            .navbar a:hover{
                color: #7ad1ff;
            }
        </style>
    </head>
<body>
<div class="full-height">
    <div class="content">
        <div class="navbar">
            <a href="/">Home</a>
        </div>
                <div class="title m-b-md">
                    <h4>{{$property->title}}</h4>
                </div>
                <div class="property-details">
                    <h4>Type: {{$property->type}}</h4>
                </div>
                <section class="content-wrapper">
                    <div class="wrapper">
                        <h4>Reviews:</h4>
                    </div>
                    <!-- Create forloop here for rendering the reviews -->
                    @foreach($reviews as $review)
                    <div class="review">
                        <div class="review-content">
                           " {{$review->review_content}} "
                        </div>
                        <div class="review-rating">
                            {{$review->rating}} /5
                        </div>
                    </div>
                    @endforeach
                </section>
    </div>
</div>
</body>
</html>
