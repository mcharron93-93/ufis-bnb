<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>UFIS-BNB</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #f6f6f6;
                color: #006a70;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            button{
                background-color:transparent;
                border:none;
                text-decoration: underline;
                font-family: 'Nunito', sans-serif;
                font-size: 14px;
                font-weight: 600;
                color:#006a70 ;
                border-radius: 3px;
                padding: 8px;
                text-decoration:none;
                transition: all 0.4s ease 0s;
            }
            button:hover{
                background-color:#dddddd;
                color:#0066a7 ;
            }

            .full-height {
                height: 100vh;
            }

            .content {
                text-align: center;
                padding-bottom: 3rem;
            }

            .title {
                font-size: 84px;
                padding: 4rem 0rem;
            }
            .property{
                padding: 2rem 0rem;
            }
            .links > .property > a {
                color: #006a70;
                font-size: 18px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
                display: block;
                margin: 5px 0px;
                transition:all 0.4s ease 0s;
            }

            .links > .property > a:hover{
                color:#0066a7 ;
            }

            .content-wrapper{
                width: 75%;
                margin: auto;
                background-color: #fff;
                padding: 2rem 0rem;
                border-radius: 2%;
                box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
            }
            .row{
                display: flex;
                justify-content: space-around;
            }
            .property-details{
                margin: auto 33%;

            }
            .sort-filter{
                padding-bottom: 3rem;
            }
            .navbar{
                width: 100%;
                display: flex;
                background-color: #2b2a2a;
                box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
            }
            .navbar div{
                font-weight: 200;
                color: #f1f1f1;
                padding: 1rem;
                transition:all 0.4s ease 0s;
            }
            .navbar a:hover{
                color: #7ad1ff;
            }
        </style>
    </head>
    <body>
        <div class="full-height">
            <div class="content">
            <div class="navbar"><div>UFIS-BNB</div></div>
                <div class="title m-b-md">
                    Welcome to UFIS-BNB!
                </div>
                <section class="content-wrapper">
                        <div class="sort-filter">
                            <button onclick="sortByType()">
                                Sort by Type
                            </button>
                            <button onclick="sortByHighest()">
                                Sort by Highest Rating
                            </button>
                            <button onclick="sortByLowest()">
                                Sort by Lowest Rating
                            </button>
                        </div>
                    <div class="links" id="linksList">
                        @foreach($properties as $property)
                        <div class="property" id="{{$property->property_id}}">
                            <a href="/property/{{$property->property_id}}">{{ $property->title }}</a>
                            <div class="property-details row">
                                <p class="type">Type: {{$property->type}}</p>
                                <p class="rating">Rating: {{$property->rating}}</p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </section>
            </div>
        </div>
       <script type="text/javascript">

            function sortByType(){
                var propertyArray = <?php echo json_encode($properties); ?>;

                propertyArray.sort(function (a, b){
                    return a.type.localeCompare(b.type)
                })

                let template = ''
                propertyArray.forEach(p => {
                    template += `<div class="property" id="${p.property_id}">
                            <a href="/property/${p.property_id}">${ p.title }</a>
                            <div class="property-details row">
                                <p class="type">Type: ${p.type}</p>
                                <p class="rating">Rating: ${p.rating}</p>
                            </div>
                        </div>`
                })

                document.getElementById('linksList').innerHTML = template
            }

            function sortByHighest(){
                var propertyArray = <?php echo json_encode($properties); ?>;
                propertyArray.sort(function (a, b){
                    return b.rating - a.rating
                })

                let template = ''
                propertyArray.forEach(p => {
                    template += `<div class="property" id="${p.property_id}">
                            <a href="/property/${p.property_id}">${ p.title }</a>
                            <div class="property-details row">
                                <p class="type">Type: ${p.type}</p>
                                <p class="rating">Rating: ${p.rating}</p>
                            </div>
                        </div>`
                })

                document.getElementById('linksList').innerHTML = template
            }

            function sortByLowest(){
                var propertyArray = <?php echo json_encode($properties); ?>;
                propertyArray.sort(function (a, b){
                    return a.rating - b.rating
                })

                let template = ''
                propertyArray.forEach(p => {
                    template += `<div class="property" id="${p.property_id}">
                            <a href="/property/${p.property_id}">${ p.title }</a>
                            <div class="property-details row">
                                <p class="type">Type: ${p.type}</p>
                                <p class="rating">Rating: ${p.rating}</p>
                            </div>
                        </div>`
                })

                document.getElementById('linksList').innerHTML = template

            }
        </script>
    </body>
</html>
