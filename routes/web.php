<?php

use App\Property;
use App\Review;
use App\Type;
// Pulling in the model can be used as an instance for get requests with Laravel; will need models created in App to use gets for other tables

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $properties = Property::all();
    return view('welcome')->with('properties', $properties);
});

Route::get('/property/{id}', function($id){
    // Create route for single property based on id, use the first() to find first occurance of obj of id
    $property = Property::where('property_id', $id)->first();

    // Grab reviews by property_id and display them based on their rating from highest to lowest
    $reviews = Review::where('property_id', $id)->get()->sortByDesc("rating");

    // Return html template with both the single property and all reviews related to it
    return view('property', ['property' => $property, 'reviews' => $reviews]);
});

