<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Type;
use App\Review;

class Property extends Model
{
    protected $table = 'property';
    protected $primaryKey = 'id';
    // appends method
    protected $appends = [
        'type',
        'rating'
      ];

    public function getTypeAttribute()
    {
      $record = Type::where('id', '=', $this->type_id)
      ->select('type')
      ->first();

    //   echo $record;
      return $record ? $record['type'] : null;
    }

    public function getRatingAttribute()
    {
      $record = Review::where('property_id', '=', $this->property_id)
      ->select('rating', 'property_id')
      ->get();

      $counter = 0;
    foreach($record as $rating){
        // get array length
        // counter
        // add rating value to counter
        // average throug array length
        $length = count($record);
        $counter += $rating['rating'];
        $average = $counter / $length;
        }
        // echo $average;
        // Look into Eloquent methods to find average method?
      return $average ? $average : null;
    }
}
